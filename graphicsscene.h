#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>
#include <QList>
#include <QGraphicsTextItem>
#include <barrita.h>


class graphicsscene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit graphicsscene(QObject* parent=0);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    QGraphicsTextItem* texto;
    bool afirmativo;
    void advance(int phase);

private:
    QList<QPointF> points;

};

#endif // GRAPHICSSCENE_H
