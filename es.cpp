#include "es.h"

ES::ES(QGraphicsScene *scene,int idPro)
{
    std::string texto = "E/S";
    QGraphicsTextItem *text = scene->addText(QString::fromStdString(texto));
    this->scene=scene;
    this->idPro=idPro;
    this->yini=idPro*-150;
    text->setPos(-80, yini*2);
    ini=0;
    id=0;
    actual=0;
}

void ES::agregar(int proceso,int idpro, int inicial)
{
    procesos.push_back(new Barrita((-ini)*5,yini,0,30,id,ini,proceso,0,idpro,inicial));
    scene->addItem(procesos[id]);
    id++;
    ini-=(proceso);
}

void ES::agregador(std::vector<std::pair<int,std::pair<int,int>>> todos)
{
    int proceso,idpro,inicial;
    for(auto iter=todos.begin(); iter!=todos.end();iter++)
    {
        proceso=iter->first;
        idpro = iter->second.first;
        inicial = iter->second.second;
        agregar(proceso,idpro,inicial);
    }
}

