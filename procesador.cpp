#include "procesador.h"
#include <QDebug>

Procesador::Procesador(QGraphicsScene *scene,int idPro,int quantum)
{
    std::string texto = "Procesador " + std::to_string(idPro);
    QGraphicsTextItem *text = scene->addText(QString::fromStdString(texto));
    this->scene=scene;
    this->idPro=idPro;
    this->quantum=quantum;
    this->yini=idPro*-30+120;
    text->setPos(-100, yini*2);
    ini=0;
    id=0;
    actual=0;
}

void Procesador::agregar(int proceso,int idpro, int idhilo)
{
    procesos.push_back(new Barrita((-ini)*5,yini,0,30,id,ini,proceso,0,idpro,idhilo));
    scene->addItem(procesos[id]);
    id++;
    ini-=(proceso);
    //qDebug() << "ini" << ini << "\n";
    //if (quantum !=0) Quantum(quantum);
}

void Procesador::Quantum(int proceso)
{
    procesos.push_back(new Barrita((-ini)*5,yini,0,30,id,ini,proceso));
    scene->addItem(procesos[id]);
    id++;
    ini-=proceso;
    //qDebug() << "ini" << ini << "\n";
}

void Procesador::agregador(std::vector<std::pair<int,std::pair<int,int>>> todos)
{
    int proceso,idpro,idhilo;
    for(auto iter=todos.begin(); iter!=todos.end();iter++)
    {
        proceso=iter->first;
        idpro = iter->second.first;
        idhilo = iter->second.second;
        agregar(proceso,idpro,idhilo);
    }
}

