#include "graphicsscene.h"
#include <QDebug>

graphicsscene::graphicsscene(QObject* parent):
    QGraphicsScene(parent)
{
    this->setBackgroundBrush(Qt::gray);
    texto = new QGraphicsTextItem(nullptr);
    addItem(texto);
}

void graphicsscene::mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << mouseEvent->scenePos();
    QList<QGraphicsItem*> items = this->items(mouseEvent->scenePos(),Qt::IntersectsItemBoundingRect);
    if(items.size()>0)
    {
        Barrita* temp = static_cast<Barrita*>(items[0]);
        texto->setPos(mouseEvent->scenePos());
        std::string txt;
        if(temp->idpro) {
            if(temp->idpro==-2)
            {
                txt = "Quantum: " + std::to_string(temp->procesoo) + '\n';
            }else if(temp->procesoreal)
            {
                txt = "ID Proceso: " + std::to_string(temp->idpro) + '\n';
                if(temp->hilo!=-1) txt+= "ID hilo: " + std::to_string(temp->hilo) + '\n';
                txt+= "Costo: " + std::to_string(temp->procesoo) + '\n';
            }
        }else {txt=""; texto->hide();}
        texto->setHtml(QString("<div style='background:rgba(255, 255, 255, 100%);'>") + QString(QString::fromStdString(txt)) + QString("</div>"));
        texto->show();
    }else if (items.size()==0) texto->hide();

    QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void graphicsscene::advance(int phase)
{
    if(!phase) return;
    qDebug() << " chirs marica \n";
}
