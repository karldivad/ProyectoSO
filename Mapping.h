#ifndef MAPPING_H
#define MAPPING_H

#include <iostream>
#include <string>
#include "Types.h"

using namespace std;

string mappingProcessType(int i){
	if(i == PROCESO_LOTES) return "Proceso por Lotes";
	if(i == PROCESO_SISTEMA) return "Proceso de Sistema";
	if(i == PROCESO_NORMAL) return "Proceso Normal";
	if(i == HILO_SISTEMA) return "Hilo de Sistema";
	if(i == HILO_LOTES) return "Hilo por Lotes";
	if(i == HILO_NORMAL) return "Hilo Normal";
}

int getProssesCode(string word){
	if(word == "P_Lotes") return PROCESO_LOTES;
	if(word == "P_Sistema") return PROCESO_SISTEMA;
	if(word == "P_Normal") return PROCESO_NORMAL;
	if(word == "H_Lotes") return HILO_LOTES;
	if(word == "H_Sistema") return HILO_SISTEMA;
	if(word == "H_Normal") return HILO_NORMAL;
	return -1;
}

int getAlgorithmCode(string word){
	if(word == "FIFO") return FIFO;
	if(word == "PRIO") return PRIO;
	bool flag = false;
	int i = 0;
	string temp = "";
	string cuantum = "";
	for(i; i < word.size(); i++){
		if(word[i] == '-') flag = true;
		else if(!flag) temp.push_back(word[i]);
		else cuantum.push_back(word[i]);
	}
	if(flag and temp == "RR"){
		int cu = stoi(cuantum);
		if(cu != 0) return RR + cu;
	}
	return -1;
}


#endif
