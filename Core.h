#ifndef CORE_H
#define CORE_H
#include <vector>
#include "Types.h"
#include "Proceso.h"


class Core{
  public:
    Core(){}
  	Core(int hw, int CCProcess, int CCHilo);
    bool empty(){return actual == nullptr;}
    int getCCProcess(){return CCProcess;}
    int getCCHilo(){return CCHilo;}
    int getCCTimer(){return CCTimer;}
    int getTipo(){return tipo;}
    int getCCCounter(){return CCCounter;}
    int getEfectiveTime(){return efectiveTime;}
    void resetCCTimer(int CC);
    void setProceso(Proceso * pro);
    void incrementarEfectiveTime(){efectiveTime++;}
    void decreaseEfectiveTime(){efectiveTime--;}
    Proceso * decreaseCCTimer();
    Proceso * sacarProceso();
    Proceso * getProceso(){return actual;}
  private:
  	int tipo; // E/S
  	int CCProcess; // quantum segun rube
    int CCHilo;
    int CCTimer;
  	
  	int CCCounter;
  	int efectiveTime;
    Proceso * actual;
};

Core::Core(int hw, int CCProcess, int CCHilo){
	tipo = hw;
  	this->CCProcess = CCProcess;
  	this->CCHilo = CCHilo;
  	CCTimer = -1;
	actual = nullptr;
	CCCounter = 0;
	efectiveTime = 0;
}

Proceso * Core::sacarProceso(){
  Proceso * temp = actual;
  actual = nullptr;
  return temp;
}

void Core::resetCCTimer(int CC){
  if(CC == PROCESO) CCTimer = CCProcess;
  else if(CC = HILO) CCTimer = CCHilo;
}

Proceso * Core::decreaseCCTimer(){
  if(CCTimer > 0){
    CCTimer--;
    CCCounter++;
    if(CCTimer == 0){
      CCTimer = -1;
      return sacarProceso();
    } 
  } 
  else if(CCTimer == 0){
    CCTimer = -1;
    return sacarProceso();
  }
  return nullptr;
}

void Core::setProceso(Proceso * pro){
  actual = pro;
}

#endif
