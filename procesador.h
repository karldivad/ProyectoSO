#ifndef PROCESADOR_H
#define PROCESADOR_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <vector>
#include <QTimer>
#include "barrita.h"
#include <utility>

class Procesador
{
public:
    Procesador(QGraphicsScene *,int,int);
    std::vector<Barrita*> procesos;
    QGraphicsScene *scene;
    void agregar(int proceso,int idpro, int idhilo);
    void Quantum(int proceso);
    void agregador(std::vector<std::pair<int,std::pair<int,int>>>);
    int quantum;
    int yini;
    int idPro;
    int actual;
    int id;
    int ini;
//public slots:
    //void moveMe();
};

#endif // PROCESADOR_H
