#ifndef TYPES_H
#define TYPES_H

#include <iostream>
#include "Step.h"
#include <map>

using namespace std;

#define MIN_PRIORITY 0
#define MAX_PRIORITY 20

enum HW_ENUM{PROCESADOR, ENTRADA_SALIDA};

enum P_OR_H{PROCESO, HILO};
enum PROCESS_ENUM {PROCESO_LOTES,PROCESO_SISTEMA,PROCESO_NORMAL,HILO_LOTES,HILO_SISTEMA,HILO_NORMAL};
enum ALGORITMOS_ENUM {FIFO, PRIO, RR};

enum FLAG_HILOS_ENUM{HILOS_KERNEL,HILOS_LIBRERIA};

enum PACK_ENUM{PROCESO_DENTRO, CC_DENTRO, VACIO};

//tiempo, id procesador (E/S -> -1), {PACK_ENUM 0 1 2}, id proceso ( no->-1)
typedef tuple<int,vector<tuple<int,int,int>>> Pack;

typedef int AlgorithmID;
typedef int ProcessType;
typedef int HardwareID;

typedef map<string,tuple<AlgorithmID,HardwareID>> MapColas;
typedef map<ProcessType,vector<Step>> MapSteps;
typedef tuple<MapColas,MapSteps> MappingObject;
typedef tuple<int,string> MappingException;

template <typename MapIterator> 
void opIterMap(MapIterator &iter, int e){
	for(int i = 0; i < e; i++){
		++iter;
	}
}

#endif
