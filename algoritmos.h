#ifndef ALGORITMOS_H
#define ALGORITMOS_H

#include "Proceso.h"
#include <vector>
#include <algorithm>
#include "Types.h"
#include "Cola.h"

using namespace std;

typedef Proceso * (*algorithmIN)(Cola &,Proceso *);
typedef Proceso * (*algorithmOUT)(Cola &);

Proceso * FIFOin(Cola &entrada, Proceso* proceso);
Proceso * RRin(Cola &entrada, Proceso* proceso);
Proceso * PRIOin(Cola &entrada, Proceso* proceso);

Proceso* FIFOout(Cola &entrada);
Proceso* RRout(Cola &entrada);
Proceso* PRIOout(Cola &entrada);


vector<algorithmIN> A_IN = {FIFOin,PRIOin,RRin};
vector<algorithmOUT> A_OUT = {FIFOout,PRIOout,RRout};

Proceso * FIFOin(Cola &entrada, Proceso* proceso)
{
  entrada.cola.push_back(proceso);
  return nullptr;
}

Proceso* FIFOout(Cola &entrada)
{
  auto temp = entrada.cola[0];
  entrada.cola.erase(entrada.cola.begin());
  return temp;
}

Proceso * RRin(Cola &entrada, Proceso* proceso)
{
  entrada.cola.push_back(proceso);
  return nullptr;
}

Proceso* RRout(Cola &entrada)
{
  auto temp = entrada.cola[0];
  entrada.cola.erase(entrada.cola.begin());
  return temp;
}

bool funMin(Proceso* a, Proceso *b){
    return a->getPrioridad() > b->getPrioridad();
}


Proceso * PRIOin(Cola & entrada, Proceso* proceso)
{
  make_heap (entrada.cola.begin(), entrada.cola.end(), funMin);
  entrada.cola.push_back(proceso);
  push_heap(entrada.cola.begin(), entrada.cola.end(), funMin);
  return nullptr;
}

Proceso* PRIOout(Cola &entrada)
{
  auto temp = entrada.cola[0];
  pop_heap(entrada.cola.begin(), entrada.cola.end(), funMin);
  entrada.cola.pop_back();
  return temp;
}


#endif
