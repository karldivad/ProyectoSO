#include "barrita.h"
#include <QDebug>

Barrita::Barrita(int x,int y,int a,int b,int id,int ini,int proceso,int tipo,int idpro, int hilo)
{
    setRect(x,y,a,b);
    this->posix=x;
    this->posiy=y;
    this->yini=b;
    this->idpro=idpro;
    this->hilo=hilo;
    this->procesoreal=true;
    setPos(this->posix,this->posiy);
    this->id=id; this->ini=ini; this->proceso=proceso;
    this->inio=ini; this->procesoo=proceso;
    this->setBrush(QColor(130*13*(idpro+1)%255, 233*7*(idpro+1)%255, 233*5*(idpro+1)%255, 123*3*(idpro+1)%255));
    if(hilo==-1) this->setBrush(Qt::red);
    if(hilo==-3) this->setBrush(Qt::black);
    hide();
}

Barrita::Barrita(int x,int y,int a,int b,int id,int ini,int proceso)
{
    setRect(x,y,a,b);
    this->posix=x;
    this->posiy=y;
    this->yini=b;
    this->idpro=-2;
    this->hilo=-2;
    setPos(this->posix,this->posiy);
    this->id=id; this->ini=ini; this->proceso=proceso;
    this->inio=ini; this->procesoo=proceso;
    this->setBrush(QColor(130*13*(id+1)%255, 233*7*(id+1)%255, 233*5*(id+1)%255, 123*3*(id+1)%255));
    hide();
}


void Barrita::advance(int phase)
{
    if(!phase) return;
    if (ini<0 || proceso==0) {ini++; return;}
    else{
        setRect(pos().x(),pos().y(),rect().width()+10,rect().height());
        proceso--;
        show();
    }
}

void Barrita::reset()
{
    ini=inio;
    proceso=procesoo;
    setRect(posix,posiy,0,yini);
    setPos(this->posix,this->posiy);
    hide();
}
