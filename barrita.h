#ifndef BARRITA_H
#define BARRITA_H

#include<QGraphicsItem>
#include <QGraphicsScene>

class Barrita : public QGraphicsRectItem
{
public:
    Barrita(int,int,int,int,int,int,int,int,int,int); // proceso
    Barrita(int,int,int,int,int,int,int); //quantum
    void advance(int);
    void reset();
    int posix;
    int posiy;
    int id;
    int hilo;
    int idpro;
    int yini;
    int ini;
    int proceso;
    int inio;
    int procesoo;
    bool procesoreal;
    QBrush color;

};

#endif // BARRITA_H
