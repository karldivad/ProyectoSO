#include "sistema.h"
#include <QDebug>
#include "AdminProcess.h"
#include <tuple>

sistema::sistema(int NUMprocesadores, std::vector<int> quantum,QGraphicsScene *scene)
{
    actual=0;
    this->NUMprocesadores = NUMprocesadores;
    for(int i=0;i<NUMprocesadores;i++) procesadores.push_back(new Procesador(scene,i,quantum[i]));
    entradis = new ES(scene,-1);
    static AdminProcess administrador("/home/kd/Planificacion/mappingFile","/home/kd/Planificacion/processFile",NUMprocesadores,2,0,1,"/home/kd/Planificacion/log");
    this->admin = (void *)&administrador;
    scene->addItem(this);
}


void sistema::agregar(int IDproceso, int procesador, int idhilo, int proceso)
{
    procesadores[procesador]->agregar(proceso,idhilo,IDproceso);
}


void sistema::agregador(int procesador, std::vector<std::pair<int,std::pair<int,int>>> todos)
{
    if (procesador==-1) entradis->agregador(todos);
    else procesadores[procesador]->agregador(todos);
}

void sistema::advance(int phase)
{
    if(!phase) return;
    AdminProcess *temp = (AdminProcess*)admin;
    Pack tmp = temp->execute();
    if(get<0>(tmp))
    {
    if(get<0>(tmp) != -1)
    {
        int tiempo = get<0>(tmp);
        int idprocesador = 0;
        int idproceso = 0;
        int estadoactual = 0;
        vector<tuple<int,int,int>> vectoriTuple = get<1>(tmp);
        for(int i=0; i<vectoriTuple.size(); i++)
        {
            idprocesador=get<0>(vectoriTuple[i]);
            estadoactual=get<1>(vectoriTuple[i]);
            idproceso=get<2>(vectoriTuple[i]);
            if(idprocesador==-1)
            {
                if(estadoactual==2)
                {
                  entradis->agregar(1,-3,0);
                }else{
                    entradis->agregar(1,idproceso,0);
                }
            }
            if(idprocesador>=0)
            {
                if(estadoactual==2)
                {
                    agregar(-3,idprocesador,idproceso,1);
                }else{
                    agregar(idproceso,idprocesador,idproceso,1);
                }
            }

            qDebug()<< "probando: " << tiempo << ' ' << idprocesador << ' ' << idproceso << ' ' << estadoactual;
        }

    }else{
        txt = new QGraphicsTextItem(nullptr);
        txt->setPlainText(QString::fromStdString(temp->getEstadistica()));
        txt->setPos(-400,0);
        scene()->addItem(txt);
    }
    }
}
