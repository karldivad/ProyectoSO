#ifndef PROCESO_H
#define PROCESO_H

#include <vector>
#include <queue>
#include <utility>
#include <iostream>
#include "Types.h"

using namespace std;

class Proceso
{
  public:
    Proceso(){padre = nullptr;}
    Proceso(int id, int tipo, int prioridad, int t_inicio, vector<int> tamanhos){
      this->id = id; 
      this->tipo = tipo; 
      this->prioridad = prioridad;
      this->t_inicio = t_inicio;
      this->tamanhos = tamanhos;
      padre = nullptr; 
      step = 0;
      vecesSacado = 0;
      tamanhoActual = 0;
      contadorTamanho = -1;
      processInitPibot = 0;
	  colaProcesadorActual = 0;
      colaESActual = 0;
	  tiempoDeSalida = 0;
    }
    Proceso(int id, int tipo, int prioridad, int t_inicio, vector<int> tamanhos, Proceso* padre){
      this->id = id; 
      this->tipo = tipo; 
      this->prioridad = prioridad;
      this->padre = padre; 
      this->t_inicio = t_inicio;
      this->tamanhos = tamanhos;
      step = 0;
      vecesSacado = 0;
      tamanhoActual = 0;
      contadorTamanho = -1;
      processInitPibot = 0;
	  colaProcesadorActual = 0;
      colaESActual = 0;
	  tiempoDeSalida = 0;
    }
    int getId(){return id;}
    int getPrioridad(){return prioridad;}
    int getT_inicio(){return t_inicio;}
    int getTipo(){return tipo;}
    int getContadorTamanho(){return contadorTamanho;}
    int getTamanhoActual(){return tamanhoActual;}
    int getTSalida(){return tiempoDeSalida;}
    int PPID();
    Proceso * getPadre(){return padre;}
   	vector<int> getNumColas(ProcessType id);
    vector<int> getTamanhos(){return tamanhos;}
    vector<Proceso*> getHilos(){return hilos;}
    void decreaseCTamanho();
    void setContadorTamanho();
    void contadorTamanhoNull(){contadorTamanho = -1;}
    void setHilos(vector<Proceso*> h){this->hilos = h;}
    void setEntrada(int in){tiemposEntrada.push_back(in);}
    void setSalida(int out);
    void incrementarContadorTamanho(){contadorTamanho++;}
    vector<int> getEntradas(){return tiemposEntrada;}
    vector<int> getSalidas(){return tiemposSalida;}
    bool termino(){return tamanhoActual == tamanhos.size();}
    bool tieneHilos(){return hilos.size() > 0;}
    bool esHilo(){return padre != nullptr;}
    bool faltanHilosInit();
    int incrementarTamanhoActual(){tamanhoActual++;}
    void incrementarTSalida(){tiempoDeSalida++;}
    int getSumTamanhos();
    void setTamanhosPadre();

    int step;
    int vecesSacado;

    int processInitPibot;
	int colaProcesadorActual;
	int colaESActual;

	int tiempoDeSalida;

  private:
    int id;
    int tipo;
    int prioridad;
    int t_inicio;
    int tamanhoActual;
    int contadorTamanho;
    
    Proceso * padre;
    vector<int> tamanhos;
    vector<Proceso*> hilos;
    vector<int> tiemposEntrada; 
    vector<int> tiemposSalida;
};

int Proceso::getSumTamanhos(){
	int res = 0;
	for(int i = 0; i < tamanhos.size(); i++){
		res += tamanhos[i];
	}
	return res;
}

void Proceso::setTamanhosPadre(){
	if(tieneHilos()){
		int sum = 0;
		for(int i = 0; i < hilos.size(); i++){
			hilos[i]->setTamanhosPadre();
			sum += hilos[i]->getSumTamanhos();
		}
		tamanhos.clear();
		tamanhos.push_back(sum);
	}
}

int Proceso::PPID(){
	if(padre == nullptr) return -1;
	return padre->getId();
}

bool Proceso::faltanHilosInit(){
	if(processInitPibot != hilos.size()) return true;
	for(auto iter = hilos.begin(); iter != hilos.end(); ++iter){
		if((*iter)->faltanHilosInit()) return true;
	}
	return false;
}

void Proceso::decreaseCTamanho(){
	if(contadorTamanho > 0) contadorTamanho--;
	if(padre != nullptr) padre->decreaseCTamanho();
}

void Proceso::setContadorTamanho(){
  if(contadorTamanho == -1){
    contadorTamanho = tamanhos[tamanhoActual];
  }
}

void Proceso::setSalida(int out){
	tiemposSalida.push_back(out);
}

vector<int> Proceso::getNumColas(ProcessType type){
	vector<int> res;
	int sum = 0;
	int flag = 0;
	for(auto iter = hilos.begin(); iter != hilos.end(); ++iter){
		if((*iter)->getTipo() == type){
			if(flag == 0){
				res.push_back(id);
				flag = 1;
			}
		}
		if((*iter)->tieneHilos()){
			vector<int> temp = (*iter)->getNumColas(type);
			res.insert(res.end(),temp.begin(),temp.end());
		}
	}
	return res;
}

#endif
