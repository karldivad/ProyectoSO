#-------------------------------------------------
#
# Project created by QtCreator 2016-11-28T11:21:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Planificacion
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    barrita.cpp \
    procesador.cpp \
    sistema.cpp \
    es.cpp \
    graphicsscene.cpp \
    test.cpp \
    proceso.cpp

HEADERS  += mainwindow.h \
    barrita.h \
    procesador.h \
    sistema.h \
    es.h \
    graphicsscene.h \
    AdminProcess.h \
    algoritmos.h \
    Cola.h \
    Core.h \
    LectorPatron.h \
    Mapping.h \
    Proceso.h \
    RootAdminProcess.h \
    Step.h \
    Types.h

FORMS    += mainwindow.ui

DISTFILES += \
    Mapeo/runTest \
    Mapeo/tt \
    Planificacion.pro.user \
    mappingFile \
    processFile
