#ifndef SISTEMA_H
#define SISTEMA_H
#include <vector>
#include "procesador.h"
#include "es.h"
#include <utility>
#include <QGraphicsScene>
#include <QGraphicsItem>
//#include "AdminProcess.h"

class sistema : public QGraphicsRectItem
{
public:
    sistema(int procesadores, std::vector<int> quantum,QGraphicsScene *scene);
    void agregar(int IDproceso, int procesador, int idhilo, int proceso);
    void agregador(int procesador, std::vector<std::pair<int,std::pair<int,int>>> todos);
    QGraphicsTextItem *txt;
    int NUMprocesadores;
    void advance(int);
    ES * entradis;
    std::vector<Procesador*> procesadores;
    void *admin;
    int actual;
};

#endif // SISTEMA_H
