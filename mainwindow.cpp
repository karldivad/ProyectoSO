#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new graphicsscene(this);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    QStandardItemModel *model = new QStandardItemModel(6,1);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Factores(comas)")));


    // 3 procesadores, {quantum respectivos}, escena)
    Guindous = new sistema(2, {1,2} ,scene);
    procesadores = Guindous->procesadores;

    /*std::vector<std::pair<int,std::pair<int,int>>> chistema =
    {
        //proceso, {idpro,idhilo}
        {10,{0,-1}},
        {20,{1,-1}},
        {3,{2,-1}}
    };

    std::vector<std::pair<int,std::pair<int,int>>> chistema1 =
    {
        //proceso, {idpro,idhilo}
        {5,{0,-1}},
        {10,{1,-1}},
        {9,{2,-1}}
    };

    std::vector<std::pair<int,std::pair<int,int>>> chistema2 =
    {
        //proceso, {idpro,idhilo}
        {5,{0,-1}},
        {5,{1,-1}},
        {5,{2,-1}},
        {5,{3,-1}},
        {5,{4,-1}}
    };

    std::vector<std::pair<int,std::pair<int,int>>> chistema3 =
    {
        //proceso, {idpro,idhilo}
        {7,{3,-1}},
        {3,{4,-1}},
        {8,{5,-1}}
    };

    std::vector<std::pair<int,std::pair<int,int>>> estest =
    {
        //proceso, {idpro,inicial}
        {7,{0,10}},
        {5,{1,20}}
    };*/

    // procesador, vector bonito
    /*Guindous->agregador(0,chistema);
    Guindous->agregador(1,chistema1);
    Guindous->agregador(2,chistema2);
    Guindous->agregador(3,chistema3);
    Guindous->agregador(-1,estest);*/

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),scene, SLOT(advance()));
    timer->start(50);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    timer->stop();
}

void MainWindow::on_pushButton_3_clicked()
{
    timer->start();
}

void MainWindow::on_pushButton_clicked()
{
    for(int i=0; i<(int)procesadores.size(); i++)
    {
        std::vector<Barrita*> procesos = procesadores[i]->procesos;
        for(int j=0; j<procesos.size(); j++) procesos[j]->reset();
    }
    std::vector<Barrita*> entrayis = Guindous->entradis->procesos;
    for(int k=0; k<entrayis.size(); k++) entrayis[k]->reset();
}
