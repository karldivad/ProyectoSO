#ifndef ROOTADMINPROCESS_H
#define ROOTADMINPROCESS_H

#include <iostream>
#include "AdminProcess.h"

class RootAdminProcess{
	public:
        RootAdminProcess(){_actualId = 0; ES = nullptr;}
		RootAdminProcess(string file, int procesadores , int numProcess, int CCProcess, string log);
		RootAdminProcess(string fileM, string fileP, int procesadores, int CCProcess, string log);
		~RootAdminProcess(){
			archivoLogs.close();
		}
	private:
		int _actualId;
		vector<Proceso *> procesos;
		vector<Core *> procesadores;
		Core * ES;
		ApColas colasProcesador;
		ApColas colasES;
		ofstream archivoLogs;
		vector<Proceso *> randomProcess(int num);

};

RootAdminProcess::RootAdminProcess(string file, int procesadores, int numProcess, int CCProcess, string log){
	_actualId = 0;
	mpO = mappingFile(file);
	procesos = randomProcess(numProcess);
	for(int i = 0; i < procesadores; i++){
		this->procesadores.push_back(new Core(PROCESADOR,CCProcess,0));
	}
	ES = new Core(ENTRADA_SALIDA,0,0);
	createColas();
	sharedTime = new int();
	*sharedTime = 0;
	processInitPibot = 0;
	colaProcesadorActual = 0;
	colaESActual = 0;
	archivoLogs.open(log.c_str());
	writeProcesos();
}

RootAdminProcess::RootAdminProcess(string fileM, string fileP, int procesadores, int CCProcess, string log){
	_actualId = 0;
	mpO = mappingFile(fileM);
	procesos = mappingProcesos(fileP);
	for(int i = 0; i < procesadores; i++){
		this->procesadores.push_back(new Core(PROCESADOR,CCProcess,0));
	}
	ES = new Core(ENTRADA_SALIDA,0,0);
	createColas();
	//for(auto iter = colasES.begin(); iter != colasES.end(); ++iter){
	//	cout<<"ENTRO"<<endl;
	//	cout<<
	//}
	sharedTime = new int();
	*sharedTime = 0;
	processInitPibot = 0;
	colaProcesadorActual = 0;
	colaESActual = 0;
	archivoLogs.open(log.c_str());
	writeProcesos();
}

vector<Proceso*> RootAdminProcess::randomProcess(int num){
	srand(time(NULL));
	vector<Proceso*> res;
	for(int i = 0; i < num; i++){
		int tipo = rand() % (PROCESO_NORMAL + 1);
		int prioridad = rand() % (MAX_PRIORITY + 1);
		int ini = rand() % (MAX_T_INI + 1);
		vector<int> tamanhos;
		int ts = rand() % MAX_TS + 1;
		for(int j = 0; j < ts; j++) tamanhos.push_back(rand() % MAX_T + 1);
		Proceso * p = new Proceso(_actualId, tipo, prioridad, ini, tamanhos);
		res.push_back(p);
		_actualId++;
	}
	sort(res.begin(), res.end(), funComp);
	return res;
}

#endif
