#ifndef COLA_H
#define COLA_H

#include <iostream>
#include "Proceso.h"

class Cola{
	public:
		Cola(){
			cola = vector<Proceso*>();
		};
		Cola(string name, int hw, int algoritmo, int cuantum){
			this->name = name;
			this->hw = hw;
			this->algoritmo = algoritmo;
			this->cuantum = cuantum;
		}
		vector<Proceso *> cola;
		string getName(){return name;};
		int getHW(){return hw;};
		int getAlgorithm(){return algoritmo;};
		int getCuantum(){return cuantum;};
	private:
		string name;
		int hw;
		int algoritmo;
		int cuantum;
};

#endif