#ifndef ES_H
#define ES_H


#include <QGraphicsItem>
#include <QGraphicsScene>
#include <vector>
#include <QTimer>
#include "barrita.h"
#include <utility>

class ES
{
public:
    ES(QGraphicsScene *,int);
    std::vector<Barrita*> procesos;
    QGraphicsScene *scene;
    void agregar(int proceso,int idpro, int inicial);
    void agregador(std::vector<std::pair<int,std::pair<int,int>>>);
    int yini;
    int idPro;
    int actual;
    int id;
    int ini;
};

#endif // ES_H
