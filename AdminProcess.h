#ifndef ADMINPROCESS_H
#define ADMINPROCESS_H

#include <iostream>
#include <stdlib.h>
#include <tuple>
#include <time.h>
#include <fstream>
#include "Core.h"
#include "Cola.h"
#include "LectorPatron.h"
#include "Mapping.h"
#include "Types.h"
#include "algoritmos.h"
#include "Proceso.h"
#include <QDebug>

#define MAX_T_INI 50
#define MAX_TS 10
#define MAX_T 500
#define PRINCIPAL -1

using namespace std;

typedef map<int,map<string,Cola>> ApColas; 

class AdminProcess{
	public:
<<<<<<< HEAD:AdministradorProcesos/AdminProcess.h
		AdminProcess(){_actualId = 0; ES = nullptr;};
		AdminProcess(string file, int procesadores , int numProcess, int CCProcess,int CCHilo, int flagHilos, string log);
=======
        AdminProcess(){_actualId = 0; ES = nullptr;}
		AdminProcess(string file, int procesadores , int numProcess, int CCProcess, string log);
>>>>>>> 119020aa7dbcb52094f3a316092e994092da22bc:AdminProcess.h
		AdminProcess(string fileM, string fileP, int procesadores, int CCProcess,int CCHilo,int flagHilos, string log);
		~AdminProcess(){
			archivoLogs.close();
        };
		void _execute();
		Pack execute();
<<<<<<< HEAD:AdministradorProcesos/AdminProcess.h
		string getEstadistica();
=======
        string getEstadistica();
>>>>>>> 119020aa7dbcb52094f3a316092e994092da22bc:AdminProcess.h
	private:
		int _actualId;
		vector<Proceso *> procesos;
		vector<Core *> procesadores;
		Core * ES;
		ApColas colasProcesador;
		ApColas colasES;
		ofstream archivoLogs;	
		MappingObject mpO;
		int sharedTime;
		int processInitPibot;
		int colaProcesadorActual;
		int colaESActual;
		bool flagHilos;
		vector<Proceso *> randomProcess(int num);
		tuple<int,string,int> getCola(Proceso * proceso);
		Pack getPack();
		void createColas();
		void _createCola(int processId, string nameCola, tuple<AlgorithmID,HardwareID> tup);
		void decrementarCCs();
		void _decrementarCCs(Core * core);
		void decrementarProcesos();
		void verificarProcesos();
		void verificarInitProcesos();
		void _verificarInitProcesos(int & processInitPibot, vector<Proceso*> procesos);
		void __verificarInitProcesos(Proceso * proceso);
		void incrementarStep(Proceso * proceso);
		void incrementarStep(Proceso * proceso, bool flag);
		void writeProcesos();
		void writeEstado();
		void writeProceso(Proceso * pro);
		void writeStateProceso(Proceso * pro);
		void writeEstadisticas();
		void _verificarCoreProcesos(Core * core, Proceso * pro);
		void _verificarProcesos(Core * core, Proceso * pro);
		void _agregarProcesoACola(Core * core, Proceso * temp);
		void __agregarProcesoACola(Core * core, Proceso * temp);
		void kill(int id);
		Proceso * agregarProcesoACola(Proceso * proceso);
		bool verificarCore(Core * core, ApColas & colas, int & indexActual);
		bool _verificarCore(Core * core, ApColas & colas, int &indexActual, int idProceso);
		bool hayProcesosEnColas();
		bool _hayProcesosEnColas(Core * core, int idProceso);
		bool _hayProcesosEnColas(ApColas & colas, int idProceso);
		bool hayProcesosEnColasProcesador();
		bool hayProcesosEnColasES();
		bool fin();
		int hayProcesadorVacio();

};

<<<<<<< HEAD:AdministradorProcesos/AdminProcess.h
AdminProcess::AdminProcess(string file, int procesadores, int numProcess, int CCProcess,int CCHilo,int flagHilos, string log){
=======
string AdminProcess::getEstadistica(){
    string res = "";
    res += "ESTADISTICAS ESTADO T: \n";
    res += "TIEMPOS DE ESPERA\n";
    char pp = 37;
    int tiemposEspera = 0;
    for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
        int sum = 0;
        int last = sharedTime;
        if((*iter)->termino()) last = (*iter)->getSalidas().back();
        vector<int> entradas = (*iter)->getEntradas();
        vector<int> salidas = (*iter)->getSalidas();
        for(int i = 0; i < (*iter)->getSalidas().size(); i++){
            sum += salidas[i] - entradas[i];
        }
        res += "<";
        res += to_string((*iter)->getId());
        res += ">: ";
        int tE = last - sum;
        res += to_string(tE);
        res += "\n";
        tiemposEspera += tE;
    }
    float promTE = (float) tiemposEspera / procesos.size();
    res += "Promedio->";
    res += to_string(promTE);
    res += "\n";
    res += "TIEMPOS DE RETORNO \n";
    int tiempoRetorno = 0;
    for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
        res += "<";
        res += to_string((*iter)->getId());
        res += ">:";
        if((*iter)->termino()){
            int tR = (*iter)->getSalidas().back() - (*iter)->getT_inicio();
            res += to_string(tR);
            res += "\n";
            tiempoRetorno += tR;
        }
        else{
            res += " PROCESO SIN TERMINAR \n";
        }
    }
    float promTR = (float) tiempoRetorno / procesos.size();
    res += "Promedio->";
    res += to_string(promTR);
    res += "\n";
    res += "TIEMPO USO EFECTIVO DEL CPU \n";
    for(int i = 0; i < procesadores.size(); i++){
        res += "<";
        res += to_string(i);
        res += ">: ";
        res += to_string(procesadores[i]->getEfectiveTime());
        res += "  ";
        res += to_string((float)(procesadores[i]->getEfectiveTime() * 100) / sharedTime);
        res += pp;
        res += "\n";
    }
    res += "TIEMPO USO EFECTIVO DE ENTRADA SALIDA->";
    res += to_string(ES->getEfectiveTime());
    res += "  ";
    res += to_string((float)(ES->getEfectiveTime() * 100) / sharedTime);
    res += pp;
    res += "\n";
    res += "OVERHEAD CPU \n";
    for(int i = 0; i < procesadores.size(); i++){
        res += "<";
        res += to_string(i);
        res += ">: ";
        res += to_string(procesadores[i]->getCCCounter());
        res += "  ";
        res += to_string((float)(procesadores[i]->getCCCounter() * 100) / sharedTime);
        res += " ";
        res += pp;
        res += "\n";
    }
    res += "OVERHEAD E/S->";
    res += to_string(ES->getCCCounter());
    res += "  ";
    res += to_string((float)(ES->getCCCounter() * 100) / sharedTime);
    res += "  ";
    res += pp;
    res += "\n";
    res += "TIEMPO USO DEL CPU \n";
    for(int i = 0; i < procesadores.size(); i++){
        res += "<";
        res += to_string(i);
        res += ">: ";
        res += to_string(procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter());
        res += "  ";
        res += to_string((float)((procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter()) * 100) / sharedTime);
        res += "  ";
        res += pp;
        res += "\n";
    }
    res += "TIEMPO USO DE ENTRADA SALIDA->";
    res += to_string(ES->getEfectiveTime() + ES->getCCCounter());
    res += "  ";
    res += to_string((float)((ES->getCCCounter() + ES->getEfectiveTime()) * 100) / sharedTime);
    res += "  ";
    res += pp;
    res += "\n";
    return res;
}

AdminProcess::AdminProcess(string file, int procesadores, int numProcess, int CCProcess, string log){
>>>>>>> 119020aa7dbcb52094f3a316092e994092da22bc:AdminProcess.h
	_actualId = 0;	
	this->flagHilos = flagHilos;
	mpO = mappingFile(file);
	procesos = randomProcess(numProcess);
	for(int i = 0; i < procesos.size(); i++){
		procesos[i]->setTamanhosPadre();
	}
	for(int i = 0; i < procesadores; i++){
		this->procesadores.push_back(new Core(PROCESADOR,CCProcess,CCHilo));
	}
	ES = new Core(ENTRADA_SALIDA,0,0);
	createColas();
	sharedTime = 0;
	processInitPibot = 0;
	colaProcesadorActual = 0;
	colaESActual = 0;
	archivoLogs.open(log.c_str());
	writeProcesos();
}

AdminProcess::AdminProcess(string fileM, string fileP, int procesadores, int CCProcess, int CCHilo,int flagHilos, string log){
	_actualId = 0;
	
	this->flagHilos = flagHilos;
	mpO = mappingFile(fileM);
	procesos = mappingProcesos(fileP);
	for(int i = 0; i < procesos.size(); i++){
		procesos[i]->setTamanhosPadre();
	}
	for(int i = 0; i < procesadores; i++){
		this->procesadores.push_back(new Core(PROCESADOR,CCProcess,CCHilo));
	}
	ES = new Core(ENTRADA_SALIDA,0,0);
	createColas();
	sharedTime = 0;
	processInitPibot = 0;
	colaProcesadorActual = 0;
	colaESActual = 0;
	archivoLogs.open(log.c_str());
	writeProcesos();
}

void AdminProcess::kill(int id){
	bool flag = false;
	for(int i = 0; i < procesadores.size(); i++){
		if(!procesadores[i]->empty() and procesadores[i]->getCCTimer() == -1 and procesadores[i]->getProceso()->getId() == id){
			Proceso * pro = procesadores[i]->sacarProceso();
			if(pro->tieneHilos()){
				for(int j = 0; j < pro->getHilos().size(); j++){
					kill(pro->getHilos()[j]->getId());
				}
			}
			flag = true;
			break;
		}
	}
	if(!flag){
		if(!ES->empty() and ES->getCCTimer() == -1 and ES->getProceso()->getId() == id){
			Proceso * pro = ES->sacarProceso();
			if(pro->tieneHilos()){
				for(int i = 0; i < pro->getHilos().size(); i++){
					kill(pro->getHilos()[i]->getId());
				}
			}
			flag = true;
		}
	}
}

bool AdminProcess::hayProcesosEnColas(){
	for(auto iter = colasProcesador.begin(); iter != colasProcesador.end(); ++iter){
		for(auto iter2 = iter->second.begin(); iter2 != iter->second.end(); ++iter2){
			if(iter2->second.cola.size() != 0){
				for(auto iter3 = iter2->second.cola.begin(); iter3 != iter2->second.cola.end(); ++iter3){
					if(!(*iter3)->termino()) return true;
				}
			}
		}
	}
	for(auto iter = colasES.begin(); iter != colasES.end(); ++iter){
		for(auto iter2 = iter->second.begin(); iter2 != iter->second.end(); ++iter2){
			if(iter2->second.cola.size() != 0) {
				for(auto iter3 = iter2->second.cola.begin(); iter3 != iter2->second.cola.end(); ++iter3){
					if(!(*iter3)->termino()) return true;
				}
			}
		}	
	}
	return false;
}

bool AdminProcess::_hayProcesosEnColas(Core * core, int idProceso){
	if(core->getTipo() == PROCESADOR) return _hayProcesosEnColas(colasProcesador,idProceso);
	else if(core->getTipo() == ENTRADA_SALIDA) return _hayProcesosEnColas(colasES,idProceso);
	return hayProcesosEnColas();
}

bool AdminProcess::_hayProcesosEnColas(ApColas & colas, int idProceso){
	for(auto iter = colas[idProceso].begin(); iter != colas[idProceso].end(); ++iter){
		if(iter->second.cola.size() != 0) return true;
	}
	return false;
}

bool AdminProcess::fin(){
	if(processInitPibot != procesos.size()) return false;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		if((*iter)->tieneHilos() and (*iter)->faltanHilosInit()) return false;
	}
	if(!ES->empty()) return false;
	for(auto iter = procesadores.begin(); iter != procesadores.end(); ++iter){
		if(!(*iter)->empty()) return false;
	}
	if(hayProcesosEnColas()) return false;
	return true;
}

void AdminProcess::writeProceso(Proceso * pro){
	if(pro == nullptr) return;
	archivoLogs<<pro->getId()<<" "<<mappingProcessType(pro->getTipo())<<" ";
	archivoLogs<<pro->getPrioridad()<<" "<<pro->getT_inicio()<<" 	";
	for(int i = 0; i < pro->getTamanhos().size(); i++){
		archivoLogs<<pro->getTamanhos()[i]<<"-";
	}
	archivoLogs<<endl;
	if(pro->tieneHilos()){
		archivoLogs<<"<HILOS>"<<endl;
		for(int i = 0; i < pro->getHilos().size(); i++){
			writeProceso(pro->getHilos()[i]);
		}
		archivoLogs<<"</HILOS>"<<endl;
	}
}

void AdminProcess::writeStateProceso(Proceso * pro){
	if(pro == nullptr) return;
	archivoLogs<<"ID  "<<"TAREA ACTUAL "<<" TAM TAREA ACTUAL"<<endl;;
	archivoLogs<<pro->getId()<<" "<<pro->getTamanhoActual()<<" "<<pro->getContadorTamanho()<<endl;
}

void AdminProcess::writeProcesos(){
	archivoLogs<<"PROCESOS"<<endl;
	archivoLogs<<"ID  TIPO       PRIORIDAD  TIEMPO INICIO   TAREAS"<<endl;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		writeProceso(*iter);
	}	
}

string AdminProcess::getEstadistica(){
	string res = "";
	res += "ESTADISTICAS ESTADO T: \n";
	res += "TIEMPOS DE ESPERA\n";
	char pp = 37;
	int tiemposEspera = 0;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		int sum = 0;
		int last = sharedTime;
		if((*iter)->termino()) last = (*iter)->getSalidas().back();
		vector<int> entradas = (*iter)->getEntradas();
		vector<int> salidas = (*iter)->getSalidas();
		for(int i = 0; i < (*iter)->getSalidas().size(); i++){
			sum += salidas[i] - entradas[i];
		}
		res += "<";
		res += to_string((*iter)->getId());
		res += ">: ";
		int tE = last - sum;
		res += to_string(tE);
		res += "\n";
		tiemposEspera += tE;
	}
	float promTE = (float) tiemposEspera / procesos.size();
	res += "Promedio->";
	res += to_string(promTE);
	res += "\n";
	res += "TIEMPOS DE RETORNO \n";
	int tiempoRetorno = 0;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		res += "<";
		res += to_string((*iter)->getId());
		res += ">:";
		if((*iter)->termino()){
			int tR = (*iter)->getSalidas().back() - (*iter)->getT_inicio();
			res += to_string(tR);
			res += "\n";
			tiempoRetorno += tR;
		}
		else{
			res += " PROCESO SIN TERMINAR \n";
		}
	}
	float promTR = (float) tiempoRetorno / procesos.size();
	res += "Promedio->";
	res += to_string(promTR);
	res += "\n";
	res += "TIEMPO USO EFECTIVO DEL CPU \n";
	for(int i = 0; i < procesadores.size(); i++){
		res += "<";
		res += to_string(i);
		res += ">: ";
		res += to_string(procesadores[i]->getEfectiveTime());
		res += "  ";
		res += to_string((float)(procesadores[i]->getEfectiveTime() * 100) / sharedTime);
		res += pp;
		res += "\n";
	}
	res += "TIEMPO USO EFECTIVO DE ENTRADA SALIDA->";
	res += to_string(ES->getEfectiveTime());
	res += "  ";
	res += to_string((float)(ES->getEfectiveTime() * 100) / sharedTime);
	res += pp;
	res += "\n";
	res += "OVERHEAD CPU \n";
	for(int i = 0; i < procesadores.size(); i++){
		res += "<";
		res += to_string(i);
		res += ">: ";
		res += to_string(procesadores[i]->getCCCounter());
		res += "  ";
		res += to_string((float)(procesadores[i]->getCCCounter() * 100) / sharedTime);
		res += " ";
		res += pp;
		res += "\n";
	}
	res += "OVERHEAD E/S->";
	res += to_string(ES->getCCCounter());
	res += "  ";
	res += to_string((float)(ES->getCCCounter() * 100) / sharedTime);
	res += "  ";
	res += pp;
	res += "\n";
	res += "TIEMPO USO DEL CPU \n";
	for(int i = 0; i < procesadores.size(); i++){
		res += "<";
		res += to_string(i);
		res += ">: ";
		res += to_string(procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter());
		res += "  ";
		res += to_string((float)((procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter()) * 100) / sharedTime);
		res += "  ";
		res += pp;
		res += "\n";
	}
	res += "TIEMPO USO DE ENTRADA SALIDA->";
	res += to_string(ES->getEfectiveTime() + ES->getCCCounter());
	res += "  ";
	res += to_string((float)((ES->getCCCounter() + ES->getEfectiveTime()) * 100) / sharedTime);
	res += "  ";
	res += pp;
	res += "\n";
	return res;
}

void AdminProcess::writeEstadisticas(){
	archivoLogs<<"----------------------"<<endl;
	archivoLogs<<"ESTADISTICAS ESTADO T:"<<sharedTime<<endl;
	archivoLogs<<"TIEMPOS DE ESPERA"<<endl;
	char pp = 37;
	int tiemposEspera = 0;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		int sum = 0;
		int last = sharedTime;
		if((*iter)->termino()) last = (*iter)->getSalidas().back();
		vector<int> entradas = (*iter)->getEntradas();
		vector<int> salidas = (*iter)->getSalidas();
		for(int i = 0; i < (*iter)->getSalidas().size(); i++){
			sum += salidas[i] - entradas[i];
		}
		archivoLogs<<"<"<<(*iter)->getId()<<">: ";
		int tE = last - sum;
		archivoLogs<<tE<<endl;
		tiemposEspera += tE;
	}
	float promTE = (float) tiemposEspera / procesos.size();
	archivoLogs<<"Promedio->"<<promTE<<endl;
	archivoLogs<<"TIEMPOS DE RETORNO"<<endl;
	int tiempoRetorno = 0;
	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		archivoLogs<<"<"<<(*iter)->getId()<<">:";
		if((*iter)->termino()){
			int tR = (*iter)->getSalidas().back() - (*iter)->getT_inicio();
			archivoLogs<<tR<<endl;
			tiempoRetorno += tR;
		}
		else{
			archivoLogs<<" PROCESO SIN TERMINAR"<<endl;
		}
	}
	float promTR = (float) tiempoRetorno / procesos.size();
	archivoLogs<<"Promedio->"<<promTR<<endl;
	archivoLogs<<"TIEMPO USO EFECTIVO DEL CPU"<<endl;
	for(int i = 0; i < procesadores.size(); i++){
		archivoLogs<<"<"<<i<<">: "<<procesadores[i]->getEfectiveTime();
		archivoLogs<<"  "<<(float)(procesadores[i]->getEfectiveTime() * 100) / sharedTime<<pp<<endl;
	}
	archivoLogs<<"TIEMPO USO EFECTIVO DE ENTRADA SALIDA->";
	archivoLogs<<ES->getEfectiveTime()<<"  "<<(float)(ES->getEfectiveTime() * 100) / sharedTime<<pp<<endl;
	archivoLogs<<"OVERHEAD CPU"<<endl;
	for(int i = 0; i < procesadores.size(); i++){
		archivoLogs<<"<"<<i<<">: "<<procesadores[i]->getCCCounter();
		archivoLogs<<" "<<(float)(procesadores[i]->getCCCounter() * 100) / sharedTime<<pp<<endl;
	}
	archivoLogs<<"OVERHEAD E/S->";
	archivoLogs<<ES->getCCCounter()<<"  "<<(float)(ES->getCCCounter() * 100) / sharedTime<<pp<<endl;
	archivoLogs<<"TIEMPO USO DEL CPU"<<endl;
	for(int i = 0; i < procesadores.size(); i++){
		archivoLogs<<"<"<<i<<">: "<<procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter();
		archivoLogs<<"  "<<(float)((procesadores[i]->getEfectiveTime() + procesadores[i]->getCCCounter()) * 100) / sharedTime<<pp<<endl;
	}
	archivoLogs<<"TIEMPO USO DE ENTRADA SALIDA->"<<endl;
	archivoLogs<<ES->getEfectiveTime() + ES->getCCCounter();
	archivoLogs<<"  "<<(float)((ES->getCCCounter() + ES->getEfectiveTime()) * 100) / sharedTime<<pp<<endl;


}

void AdminProcess::writeEstado(){
	archivoLogs<<"----------------------"<<endl;
	archivoLogs<<"ESTADO T:"<<sharedTime<<endl;
	archivoLogs<<"PROCESADORES"<<endl;
	for(int i = 0; i < procesadores.size(); i++){
		archivoLogs<<"<"<<i<<"> : ";
		if(procesadores[i]->getCCTimer() == -1){
			writeStateProceso(procesadores[i]->getProceso());	
		}
		else{
			archivoLogs<<"CAMBIO DE CONTEXTO ->"<<procesadores[i]->getCCTimer()<<endl;
		}
	}
	archivoLogs<<endl;
	archivoLogs<<"ENTRADA SALIDA : ";
	if(ES->getCCTimer() == -1){
		writeStateProceso(ES->getProceso());	
	}
	else{
		archivoLogs<<"CAMBIO DE CONTEXTO ->"<<ES->getCCTimer();
	}
	archivoLogs<<endl;
	
	archivoLogs<<"COLAS DEL PROCESADOR"<<endl;
	for(auto iter = colasProcesador[PRINCIPAL].begin(); iter != colasProcesador[PRINCIPAL].end(); ++iter){
		archivoLogs<<"<"<<iter->second.getName()<<"> :";
		for(auto iter2 = iter->second.cola.begin(); iter2 != iter->second.cola.end(); ++iter2){
			archivoLogs<<(*iter2)->getId()<<"->";
		}
		archivoLogs<<endl;
	}
	archivoLogs<<"COLAS DE ENTRADA SALIDA"<<endl;
	for(auto iter = colasES[PRINCIPAL].begin(); iter != colasES[PRINCIPAL].end(); ++iter){
		archivoLogs<<"<"<<iter->second.getName()<<"> :";
		for(auto iter2 = iter->second.cola.begin(); iter2 != iter->second.cola.end(); ++iter2){
			archivoLogs<<(*iter2)->getId()<<"->";
		}
		archivoLogs<<endl;
	}
	archivoLogs<<endl;
}

int AdminProcess::hayProcesadorVacio(){
	for(int i = 0; i < procesadores.size(); i++){
		if(procesadores[i]->getCCTimer() == -1 and procesadores[i]->empty()) return i;
	}
	return -1;
}

void AdminProcess::decrementarCCs(){
	for(auto iter = procesadores.begin(); iter != procesadores.end(); ++iter){
		_decrementarCCs(*iter);
	}
	_decrementarCCs(ES);
}

void AdminProcess::__agregarProcesoACola(Core * core, Proceso * temp){
	Proceso * temp2 = nullptr;
	if(flagHilos == HILOS_LIBRERIA){
		temp2 = temp->getPadre();
		while(temp2 != nullptr){
			incrementarStep(temp2,true);
			temp2 = temp2->getPadre();
		}
		if(core->getCCProcess() > 0 and temp->getPadre()->getContadorTamanho() != 0 and !temp->termino()){	
			if(_hayProcesosEnColas(core,temp->PPID()) or temp->getContadorTamanho() == -1){
				core->resetCCTimer(PROCESO);
				temp2 = temp->getPadre();
				while(temp2->getPadre() != nullptr){
					temp2->setSalida(sharedTime + 1);
					temp2 = temp2->getPadre();
				}
				core->setProceso(temp2);
			}
		}
		else {
			temp2 = temp->getPadre();
			while(temp2->getPadre() != nullptr){
				temp2 = temp2->getPadre();
			}
			if(temp->getPadre() != temp2 or temp->getPadre()->getContadorTamanho() != 0){
				agregarProcesoACola(temp2);
			}  
		}
	}
	else{
		if(core->getCCProcess() > 0 and temp->getPadre()->getContadorTamanho() != 0 and !temp->termino() and !_hayProcesosEnColas(core,temp->PPID()) and temp->getContadorTamanho() == -1){	
			core->resetCCTimer(PROCESO);
			temp2 = temp->getPadre();
			while(temp2->getPadre() != nullptr){
				temp2->setSalida(sharedTime + 1);
				temp2 = temp2->getPadre();
			}
			core->setProceso(temp2);			
		}
		else{
			temp2 = temp->getPadre();
			int tt = core->getTipo();
			if(tt == PROCESADOR) tt = ENTRADA_SALIDA;
			else tt = PROCESADOR;
			while(temp2 != nullptr){
				temp2->step = tt;
				temp2 = temp2->getPadre();
			}
			temp2 = temp->getPadre();
			while(temp2 != nullptr){
				agregarProcesoACola(temp2);
				temp2 = temp2->getPadre();
			}		
			
			temp2 = temp->getPadre();
			while(temp2 != nullptr){
				temp2->step = core->getTipo();
				temp2 = temp2->getPadre();
			}
		}		
	}
}

void AdminProcess::_agregarProcesoACola(Core * core, Proceso * temp){
	Proceso * temp2 = nullptr;
	if(!temp->tieneHilos()) incrementarStep(temp);
	Proceso * nuevo = agregarProcesoACola(temp);
	if(temp->esHilo()){
		bool flag = true;
		if(temp->getContadorTamanho() == -1){
			__agregarProcesoACola(core,temp);
			flag = false;
			if(flagHilos == HILOS_LIBRERIA){
				temp2 = temp->getPadre();
				while(temp2->getPadre() != nullptr){
					agregarProcesoACola(temp2);
					temp2 = temp2->getPadre();
				}
				if(flag) agregarProcesoACola(temp2);
			}
			else{	
				if(core->getCCTimer() == -1){
					if(core->getTipo() == PROCESADOR) _verificarCore(core, colasProcesador,temp->getPadre()->colaProcesadorActual, temp->getPadre()->getId());
					else _verificarCore(core,colasES,temp->getPadre()->colaESActual,temp->getPadre()->getId());
					core->getProceso()->incrementarTSalida();
					if(core->getCCHilo() > 0){
						core->getProceso()->incrementarContadorTamanho();
						core->getProceso()->tiempoDeSalida = core->getProceso()->tiempoDeSalida + 1;	
						core->getProceso()->getPadre()->incrementarContadorTamanho();
						core->getProceso()->getPadre()->tiempoDeSalida = core->getProceso()->getPadre()->tiempoDeSalida + 1;
						core->decreaseEfectiveTime();
					}
				}	
			}
			
		}
		else if(flagHilos == HILO_SISTEMA){
			temp2 = temp->getPadre();
			while(temp2 != nullptr){
				temp2->step = core->getTipo();
				temp2 = temp2->getPadre();
			}
		}
		else{
			if(core->getTipo() == PROCESADOR) _verificarCore(core, colasProcesador,temp->getPadre()->colaProcesadorActual, temp->getPadre()->getId());
			else _verificarCore(core,colasES,temp->getPadre()->colaESActual,temp->getPadre()->getId());
			core->getProceso()->incrementarTSalida();
			if(core->getCCHilo() > 0){
				core->getProceso()->incrementarContadorTamanho();
				core->getProceso()->tiempoDeSalida = core->getProceso()->tiempoDeSalida + 1;
				core->getProceso()->getPadre()->incrementarContadorTamanho();
				core->getProceso()->getPadre()->tiempoDeSalida = core->getProceso()->getPadre()->tiempoDeSalida + 1;
				core->decreaseEfectiveTime();
			}
		}
		
	}
	if(nuevo != nullptr){
			//TODO
			//TODO
			//TODO
			//TODO
			//TODO
	}
}

void AdminProcess::_decrementarCCs(Core * core){
	Proceso * temp = core->decreaseCCTimer();
	if(temp != nullptr and !temp->termino()){
		_agregarProcesoACola(core,temp);
	}
}

void AdminProcess::decrementarProcesos(){
	for(auto iter = procesadores.begin(); iter != procesadores.end(); ++iter){
		if(!(*iter)->empty() and (*iter)->getCCTimer() == -1) {
			(*iter)->getProceso()->decreaseCTamanho();
			(*iter)->incrementarEfectiveTime();
		}
	}
	if(!ES->empty() and ES->getCCTimer() == -1) {
		ES->getProceso()->decreaseCTamanho();
		ES->incrementarEfectiveTime();
	}
}

void AdminProcess::incrementarStep(Proceso * proceso, bool flag){
	if(proceso->getContadorTamanho() != -1 and !flag) return;
	Step step = (get<1>(mpO))[proceso->getTipo()][proceso->step];
	if(step.bucle == -1) proceso->step += 1;
	else proceso->step = step.bucle;
}

void AdminProcess::incrementarStep(Proceso * proceso){
	if(proceso->getContadorTamanho() != -1) return;
	Step step = (get<1>(mpO))[proceso->getTipo()][proceso->step];
	if(step.bucle == -1) proceso->step += 1;
	else proceso->step = step.bucle;
}

void AdminProcess::_verificarCoreProcesos(Core * core, Proceso * pro){
	bool flag = false;
	while(pro->esHilo() and !flag){
		if(core->getTipo() == PROCESADOR) flag = _verificarCore(core,colasProcesador,pro->getPadre()->colaProcesadorActual,pro->PPID());
		else flag = _verificarCore(core,colasES,pro->getPadre()->colaESActual,pro->PPID());	
	}
}

void AdminProcess::_verificarProcesos(Core * core, Proceso * pro){
	if(pro == nullptr) return;	
	if((sharedTime == pro->getTSalida() or pro->getContadorTamanho() == 0) and core->getCCTimer() == -1){
		if(pro->getContadorTamanho() == 0){
			pro->incrementarTamanhoActual();
			pro->vecesSacado = 0;
			pro->contadorTamanhoNull();
		}
		else{
			pro->vecesSacado++;
		}
		if(!pro->termino()){
			if(pro->esHilo()){
				if(core->getCCHilo() > 0){
					if(_hayProcesosEnColas(core,pro->PPID()) or pro->getContadorTamanho() == -1){
						core->resetCCTimer(HILO);
						Proceso * temp = core->getProceso();
						while(temp != pro){
							temp->setSalida(sharedTime + 1);
							temp = temp->getPadre();
						}
						pro->setSalida(sharedTime + 1);
					}
				}
				else{
					Proceso * temp = core->sacarProceso();
					Proceso * temp2 = temp;
					while(temp != pro){
						temp->setSalida(sharedTime + 1);
						temp = temp->getPadre();
					}
					pro->setSalida(sharedTime + 1);
					if(!pro->termino()){
						_agregarProcesoACola(core,temp2);
					}
					else{
						//TODO FALTA EL STEP DEL PADRE
						_agregarProcesoACola(core,pro->getPadre());
					}
					//_verificarProcesos(core, pro);
				} 
			} 
			else{
				if(core->getCCProcess() > 0){	
					if(_hayProcesosEnColas(core,pro->PPID()) or pro->getContadorTamanho() == -1){
						core->resetCCTimer(PROCESO);
						Proceso * temp = core->getProceso();
						while(temp != pro){
							temp->setSalida(sharedTime + 1);
							temp = temp->getPadre();
						}
						pro->setSalida(sharedTime + 1);
					}
				} 
				else {
					Proceso * temp = core->sacarProceso();
					Proceso * temp2 = temp;
					while(temp != pro){
						temp->setSalida(sharedTime + 1);
						temp = temp->getPadre();
					}
					pro->setSalida(sharedTime + 1);
					if(!pro->termino()){
						_agregarProcesoACola(core,temp2);
					}
					else{
						//TODO ARRIBITAAAAA
					}
					//_verificarProcesos(core, pro);
				}
			} 
		}
		else if(_hayProcesosEnColas(core,pro->PPID()) and pro->esHilo()){
			if(core->getTipo() == PROCESADOR) _verificarCore(core, colasProcesador,pro->getPadre()->colaProcesadorActual, pro->getPadre()->getId());
			else _verificarCore(core,colasES,pro->getPadre()->colaESActual,pro->getPadre()->getId());
		}
		else{
			Proceso * temp = core->sacarProceso();			
			while(temp != pro){
				if(temp == nullptr) break;
				temp->setSalida(sharedTime + 1);
				temp = temp->getPadre();
			}
			pro->setSalida(sharedTime + 1);

			if(pro->esHilo()){
				__agregarProcesoACola(core,temp);
				Proceso * temp2 = temp->getPadre();
				while(temp2->getPadre() != nullptr){					
					if(temp->getPadre() != temp2 or temp->getPadre()->getTamanhoActual() != 0) agregarProcesoACola(temp2);
					temp2 = temp2->getPadre();
				}
				if(flagHilos == HILOS_KERNEL) agregarProcesoACola(temp2);
			}
			//_verificarCoreProcesos(core, pro);
		}
	}
	_verificarProcesos(core,pro->getPadre());
}


void AdminProcess::verificarProcesos(){
	for(auto iter = procesadores.begin(); iter != procesadores.end(); ++iter){
		if(!(*iter)->empty()) _verificarProcesos(*iter, (*iter)->getProceso());
	}
	if(!ES->empty()) _verificarProcesos(ES,ES->getProceso());

}

void AdminProcess::_createCola(int processId, string nameCola, tuple<AlgorithmID,HardwareID> tup){
	int alg = get<0>(tup);
	int cuantum = 0;
	if(alg > RR){
		cuantum = alg - RR;
		alg = RR;
	}
	if(get<1>(tup) == PROCESADOR){
		colasProcesador[processId][nameCola] = Cola(nameCola,get<1>(tup),alg,cuantum);
	}
	else if(get<1>(tup) == ENTRADA_SALIDA){
		colasES[processId][nameCola] = Cola(nameCola,get<1>(tup),alg,cuantum);
	}
}

void AdminProcess::createColas(){
	for(auto iter = (get<0>(mpO)).begin(); iter != (get<0>(mpO)).end(); ++iter){
		_createCola(PRINCIPAL,iter->first,iter->second);
	}
	for(auto iter = (get<1>(mpO)).begin(); iter != (get<1>(mpO)).end(); ++iter){
		ProcessType pT = get<0>(*iter);
		if(pT > PROCESO_NORMAL){
			for(auto iter2 = procesos.begin(); iter2 != procesos.end(); ++iter2){
				if((*iter2)->tieneHilos()){
					vector<int> temp = (*iter2)->getNumColas(pT);
					for(int i = 0; i < temp.size(); i++){
						for(auto iter3 = (get<1>(*iter)).begin(); iter3 != (get<1>(*iter)).end(); ++iter3){
							for(int j = 0; j < (*iter3).algoritmos.size(); j++){
								string colaName = (*iter3).algoritmos[j];
								_createCola(temp[i],colaName,(get<0>(mpO))[colaName]);
							}
						}
					}
				}
			}
		}
	}
}

Proceso * AdminProcess::agregarProcesoACola(Proceso * proceso){
	auto nameCola = getCola(proceso);
	Proceso * nuevo = nullptr;
	
	if(get<2>(nameCola) == PROCESADOR){
		nuevo =  A_IN[colasProcesador[get<0>(nameCola)][get<1>(nameCola)].getAlgorithm()](colasProcesador[get<0>(nameCola)][get<1>(nameCola)],proceso);
	}
	else if(get<2>(nameCola) == ENTRADA_SALIDA){
		nuevo =  A_IN[colasES[get<0>(nameCola)][get<1>(nameCola)].getAlgorithm()](colasES[get<0>(nameCola)][get<1>(nameCola)],proceso);
	}
	return nuevo;
}

vector<Proceso*> AdminProcess::randomProcess(int num){
	srand(time(NULL));
	vector<Proceso*> res;
	for(int i = 0; i < num; i++){
		int tipo = rand() % (PROCESO_NORMAL + 1);
		int prioridad = rand() % (MAX_PRIORITY + 1);
		int ini = rand() % (MAX_T_INI + 1);
		vector<int> tamanhos;
		int ts = rand() % MAX_TS + 1;
		for(int j = 0; j < ts; j++) tamanhos.push_back(rand() % MAX_T + 1);
		Proceso * p = new Proceso(_actualId, tipo, prioridad, ini, tamanhos);
		res.push_back(p);
		_actualId++;
	}
	sort(res.begin(), res.end(), funComp);
	return res;
}

tuple<int,string,int> AdminProcess::getCola(Proceso * proceso){
	int stp = proceso->vecesSacado;
	Step step = (get<1>(mpO))[proceso->getTipo()][proceso->step];
	if(proceso->vecesSacado >= step.algoritmos.size()) stp = step.algoritmos.size() - 1;
	string nameCola = step.algoritmos[stp];
	if(proceso->esHilo()) return make_tuple(proceso->PPID(),nameCola,step.hardware);
	else return make_tuple(PRINCIPAL,nameCola,step.hardware);
}

void AdminProcess::_verificarInitProcesos(int & processInitPibot, vector<Proceso*> procesos){
	for(processInitPibot; processInitPibot < procesos.size(); processInitPibot++){
		if(sharedTime == procesos[processInitPibot]->getT_inicio()){
			Proceso * nuevo = agregarProcesoACola(procesos[processInitPibot]);
			if(nuevo != nullptr){
				//TODO
				//TODO
				//TODO
				//TODO
				//TODO
			}
		}
		else break;
	}
}

void AdminProcess::__verificarInitProcesos(Proceso * proceso){
	if(proceso->tieneHilos()){
		_verificarInitProcesos(proceso->processInitPibot, proceso->getHilos());
		for(int i = 0; i < proceso->getHilos().size(); i++){
			__verificarInitProcesos(proceso->getHilos()[i]);
		}
	}
}

void AdminProcess::verificarInitProcesos(){

	_verificarInitProcesos(processInitPibot,procesos);

	for(auto iter = procesos.begin(); iter != procesos.end(); ++iter){
		__verificarInitProcesos(*iter);
	}
}

bool AdminProcess::_verificarCore(Core * core, ApColas & colas, int &indexActual, int idProceso){
	int ProTemp = indexActual;
	bool flag = false;
	while(true){
		auto iter = colas[idProceso].begin();
		opIterMap(iter,indexActual);
		if(iter->second.cola.size() != 0){
			Proceso * nuevo = A_OUT[iter->second.getAlgorithm()](iter->second);
			while(nuevo->termino()){
				nuevo = A_OUT[iter->second.getAlgorithm()](iter->second);
			}
			bool ff = false;
			if(nuevo->tieneHilos()){
				int iA = nuevo->colaProcesadorActual;
				if(core->getTipo() == ENTRADA_SALIDA) iA = nuevo->colaESActual;
				_verificarCore(core,colas,iA,nuevo->getId());
				ff = core->empty();
			}
			else{
				core->setProceso(nuevo);
			}
			if(!ff){
				int tiempoDeSalida = sharedTime;
				nuevo->setContadorTamanho();			
				nuevo->setEntrada(sharedTime);
				if(iter->second.getCuantum() == 0) tiempoDeSalida += nuevo->getContadorTamanho();
				else tiempoDeSalida += iter->second.getCuantum() - 1;
				nuevo->tiempoDeSalida = tiempoDeSalida;

			}
			if(indexActual == colas[idProceso].size()) indexActual = 0;
			else indexActual++;
			if(indexActual == colas[idProceso].size()) indexActual = 0;
			if(indexActual == ProTemp) flag = true;
			if(!ff or flag) break;
		}
		else{
			if(indexActual == colas[idProceso].size()) indexActual = 0;
			else indexActual++;
			if(indexActual == colas[idProceso].size()) indexActual = 0;
			if(indexActual == ProTemp){
				flag = true;
				break;
			}
		}	
	}
	return flag;
}

bool AdminProcess::verificarCore(Core * core, ApColas & colas, int & indexActual){
	return _verificarCore(core,colas,indexActual,PRINCIPAL);
}

Pack AdminProcess::getPack(){
	vector<tuple<int,int,int>> res;
	int flag = PROCESO_DENTRO;
	int id = -1;
	if(ES->getCCTimer() != -1) flag = CC_DENTRO;
	else if(ES->empty()) flag = VACIO;
	if(flag == PROCESO_DENTRO) id = ES->getProceso()->getId();
	res.push_back(make_tuple(-1,flag,id));
	for(int i = 0; i < procesadores.size(); i++){
		flag = PROCESO_DENTRO;
		id = -1;
		if(procesadores[i]->getCCTimer() != -1) flag = CC_DENTRO;
		else if(procesadores[i]->empty()) flag = VACIO;
		if(flag == PROCESO_DENTRO) id = procesadores[i]->getProceso()->getId();
		res.push_back(make_tuple(i,flag,id));
	}
	return make_tuple(sharedTime,res);
}

Pack AdminProcess::execute(){
<<<<<<< HEAD:AdministradorProcesos/AdminProcess.h
	if(fin()){
		return make_tuple(-1,vector<tuple<int,int,int>>());
	}
=======
    if(fin()){
        return make_tuple(-1,vector<tuple<int,int,int>>());
    }
>>>>>>> 119020aa7dbcb52094f3a316092e994092da22bc:AdminProcess.h
	verificarInitProcesos();
	if(ES->empty() and ES->getCCTimer() == -1){
		verificarCore(ES, colasES, colaESActual);
	}
	int pro;
	while((pro = hayProcesadorVacio()) != -1){
		if(verificarCore(procesadores[pro], colasProcesador, colaProcesadorActual)) break;
	}
	Pack pack = getPack();
<<<<<<< HEAD:AdministradorProcesos/AdminProcess.h
	writeEstado();
=======
    writeEstado();
>>>>>>> 119020aa7dbcb52094f3a316092e994092da22bc:AdminProcess.h
	decrementarCCs();
	decrementarProcesos();
	verificarProcesos();
	
	sharedTime++;

	return pack;
}

void AdminProcess::_execute(){
	while(!fin()){
		verificarInitProcesos();
		
		if(ES->empty() and ES->getCCTimer() == -1){
			verificarCore(ES, colasES, colaESActual);
		}
		int pro;
		while((pro = hayProcesadorVacio()) != -1){
			if(verificarCore(procesadores[pro], colasProcesador, colaProcesadorActual)) break;
		}
		
		writeEstado();
		decrementarCCs();
		decrementarProcesos();
		verificarProcesos();
		
		sharedTime++;
	}
	writeEstado();
	writeEstadisticas();
}

#endif
